# curl

```
bash <(curl -Ss https://gitlab.com/mkolakowski/curl/-/raw/master/curl.sh) $optional_flag
```
Can also add commands at the end such as


| Command | Action |
| ------ | ------ |
| update | updates OS |
| restart | restarts OS |
